var gameArray = [8, 3, 1, 2, 4, 1, 3, 2, 4, 5, 8, 5, 6, 7, 6, 7];
var pickedTiles = [];
var scoreText;
var moves = 0;
var gameLayer;
var shuffle = function(v){
    for(var j, x, i = v.length; i; j = parseInt(Math.random() * i),
        x = v[--i],
        v[i] = v[j],
        v[j] = x);
    return v;
};
/*var shuffle = function(v){
    for(var j, x, i = v.length; i >= 0;){
        j = parseInt(Math.random() * i)
        x = v[--i];
        v[i] = v[j];
        v[j] = x;
    }
    return v;
}*/
var gameScene = cc.Scene.extend({
    onEnter:function () {

        this._super();
        gameLayer = new game();
        gameLayer.init();
        this.addChild(gameLayer);
    }
});

var game = cc.Layer.extend({
    init:function(){
        this._super();
        gameArray = shuffle(gameArray);
        var size = cc.winSize;
        var layerGradient = cc.LayerGradient.create(cc.color.BLACK, new cc.Color(229,255,204,255), cc.p(2, 1),
            [{p:0, color: cc.color.BLUE},
             {p:.5, color: new cc.Color(0,0,0,0)},
             {p:1, color: cc.color.BLUE}]);
        this.addChild(layerGradient);
        scoreText = cc.LabelTTF.create("Moves = 0", "Arial", "32", cc.TEXT_ALIGNMENT_CENTER);
        this.addChild(scoreText);
        scoreText.setPosition(size.width / 2, 420);
        for(var i = 0; i < 16; i++){
            var tile = new MemoryTile();
            tile.pictureValue = gameArray[i];
            this.addChild(tile, 0);
            tile.setPosition((size.width/4 + 80) + i%4*75, 360 - Math.floor(i/4)*75);
       }
    }
});

var MemoryTile = cc.Sprite.extend({
    ctor: function(){
        this._super();
        this.initWithFile("res/tile_0.png");
        cc.eventManager.addListener(listener.clone(), this);
    }
});

var listener = cc.EventListener.create({
    event: cc.EventListener.TOUCH_ONE_BY_ONE,
    swallowTouches : true,
    onTouchBegan: function (touch, event) {
        if(pickedTiles.length < 2){
            var target = event.getCurrentTarget();
            var location = target.convertToNodeSpace(touch.getLocation());
            var targetSize = target.getContentSize();
            var targetRectangle = cc.rect(0, 0, targetSize.width, targetSize.height);
            if(cc.rectContainsPoint(targetRectangle, location)) {
                if (pickedTiles.indexOf(target) === -1) {
                    target.initWithFile("res/tile_" + target.pictureValue + ".png");
                    pickedTiles.push(target);
                    if (pickedTiles.length === 2) {
                        checkTiles();
                    }
                }
            }
        }
    }
});

function checkTiles(){
    moves++;
    scoreText.setString("Moves: " + moves);
    var pause = setTimeout(function(){
        if(pickedTiles[0].pictureValue !== pickedTiles[1].pictureValue){
            pickedTiles[0].initWithFile("res/tile_0.png");
            pickedTiles[1].initWithFile("res/tile_0.png");
        }
        else{
            gameLayer.removeChild(pickedTiles[0]);
            gameLayer.removeChild(pickedTiles[1]);
        }
        pickedTiles = [];
    }, 400);
}

